import { Hash } from "./Hash";
export class Admin {
    private passwordHash: string;
    private hash: any;
    constructor() {
        this.hash = new Hash();
        this.generateNewPw();
    }

    generateNewPw() {
        let password = '_' + Math.random().toString(36).substr(2, 9);
        console.log(password);
        this.passwordHash = this.hash.hashPW(password);
    }
    setPassword(oldPw: string, newPw: string) {
        if (this.checkPassword(oldPw)) {
            this.passwordHash = this.hash.hashPW(newPw);
            return true;
        }
        return false;
    }

    checkPassword(pw: string): boolean {
        if (this.hash.hashPW(pw) == this.passwordHash) {
            return true;
        }
        return false;
    }

}