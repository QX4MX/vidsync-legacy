export class Video{
    private id:string;
    private name:string;
    private channel:string;
    private postedTime: string;
    private views:string;
    private likes:string;
    private dislikes:string;
    constructor(id:string,name:string,channel:string,postedTime:string,views:string,likes:string,dislikes:string){
        this.id = id;
        this.name = name;
        this.channel = channel;
        this.postedTime = postedTime;
        this.views = views;
        this.likes = likes;
        this.dislikes = dislikes;
        console.log("Video ->" +id+" "+name +" "+channel+" "+postedTime+" "+views+" "+likes+" "+dislikes)
    }

    public getId():string{
        return this.id;
    }
    public getName():string{
        return this.name;
    }
    public getChannel():string{
        return this.channel;
    }
    public getPostedTime():string{
        return this.postedTime;
    }
    public getViews():string{
        return this.views;
    }
    public getLikes():string{
        return this.likes;
    }
    public getDislikes():string{
        return this.dislikes;
    }

}