import { RoomData } from './RoomData';
import { UserData } from './UserData';
import { Admin } from './Admin';
import { SystemUserName, SocketEvent, Errors } from "./Enums";
import { User } from './User';
import { Room } from './Room';
import { ChatEvents } from './ChatEvents';
import { youtubeapi } from './youtubeapi';
import { Video } from './Video';

export class UserEvents {
    private io: SocketIO.Server;
    private chat: ChatEvents;
    private rooms: RoomData;
    private users: UserData;
    private admin: Admin;
    private ytApi: youtubeapi;

    private onlineSince: Date = new Date();
    private connectedCount: number = 0;
    private alltimeVisitors: number = 0;

    constructor(io: SocketIO.Server, chat: ChatEvents, rooms: RoomData, users: UserData, ytApi: youtubeapi) {
        this.io = io;
        this.chat = chat;
        this.rooms = rooms;
        this.users = users;
        this.admin = new Admin();
        this.ytApi = ytApi;
    }

    getAdmin() {
        return this.admin;
    }

    userConnect(socket: SocketIO.Socket) {
        this.connectedCount++;
        this.alltimeVisitors++;
        let id = socket.handshake.query['userId'];
        let username = socket.handshake.query['username'];

        if (username == "Guest" || username.toUpperCase() in SystemUserName || this.users.usernameUsed(id, username)) {
            username = "Guest" + this.alltimeVisitors.toString();
            //socket.emit(SocketEvent.ALERT, Errors.USERNAMESET + '(' + username + ')');
        }
        this.users.connect(id, socket.id, username);
        let user = this.users.getUserBySocketId(socket.id);
        if (!user) {
            return
        }
        socket.emit(SocketEvent.SET_USERNAME, true, user.getUserName());
        socket.emit(SocketEvent.SET_ID, user.getUserId());
        // todo check if in index
        socket.emit(SocketEvent.PUBLIC_ROOMS, this.rooms.getPublicRooms());
    }

    userDisconnect(socket: SocketIO.Socket) {
        this.connectedCount--;
        let user: User = this.users.getUserBySocketId(socket.id);
        if (user) {
            let room: Room = this.rooms.getRoom(user.getCurrentRoom());
            this.users.disconnect(user);
            if (room) {
                room.userDisconnect(user);
                if (room.getIsPublic()) {
                    // todo (dont send to all)
                    this.io.emit(SocketEvent.PUBLIC_ROOMS, this.rooms.getPublicRooms());
                }
                this.io.to(room.getName()).emit(SocketEvent.UPDATE, room.getUserCount(), room.getName());
                this.chat.roomMsg(room.getName(), user.getUserName() + " left room", SystemUserName.ROOM);
            }
        }
        this.rooms.deleteInactiveRooms();
    }

    userSetUserName(socket: SocketIO.Socket, newUserName: string) {
        let user = this.users.getUserBySocketId(socket.id);
        if (!user) {
            return
        }
        let id = user.getUserId();
        if (!this.users.usernameUsed(id, newUserName) && !(/[^\w.]/.test(newUserName)) && !(newUserName.toUpperCase() in SystemUserName) && !(!newUserName || newUserName.length === 0 || newUserName.length > 20 || newUserName.charAt(0) === "")) {

            let oldUserName = user.getUserName();
            user.setUserName(newUserName);
            this.rooms.updateRoomOwnerUserName(newUserName, oldUserName);
            socket.emit(SocketEvent.SET_USERNAME, true, newUserName);
            this.chat.socketMsg(socket, "Your new UserName is " + newUserName, null);
            this.chat.roomMsg(user.getCurrentRoom(), oldUserName + ' changed UserName to ' + newUserName, null);
        } else {
            socket.emit(SocketEvent.ALERT, Errors.NAMENOTALLOWED);
        }

    }

    userJoinRoom(socket: SocketIO.Socket, roomID: string, isPerm: boolean, isPublic: boolean, isEditable: boolean) {
        let user = this.users.getUserBySocketId(socket.id);
        if (!user) {
            return
        }
        let oldRoomId = user.getCurrentRoom();
        if ((/[^\w.]/.test(roomID)) || !roomID || roomID.length === 0 || roomID.length > 20 || roomID.charAt(0) === "") {
            socket.emit(SocketEvent.BACKTOINDEX);
        } else if (oldRoomId != roomID) {
            this.userLeaveRoom(socket, oldRoomId);
            let room = this.rooms.createRoomIfNotExisting(roomID, user.getUserName(), isPerm, isPublic, isEditable, null);
            socket.join(roomID, () => {
                user.setCurrentRoom(roomID);
                room.userConnect(user);
                // load Video and Queue
                socket.emit(SocketEvent.LOAD_VID, room.getCurrentUrl());
                let video = room.getCurrentVideo();
                if(video){
                    let vidInfo:Array<string> = new Array<string>(video.getName(),video.getChannel(),video.getViews(),video.getLikes(),video.getDislikes(),video.getPostedTime());
                    socket.emit(SocketEvent.VIDSTATS, vidInfo);
                }
                socket.emit(SocketEvent.LOAD_QUEUE, room.getQueue(), room.getQueueCount());
                socket.emit(SocketEvent.UPDATE, room.getUserCount(), user.getCurrentRoom());
                if (room.getIsPublic()) {
                    // todo (dont send to all)
                    this.io.emit(SocketEvent.PUBLIC_ROOMS, this.rooms.getPublicRooms());
                }
                this.io.to(roomID).emit(SocketEvent.UPDATE, room.getUserCount(), roomID);
                this.chat.roomMsg(roomID, user.getUserName() + " joined Room (" + roomID + ")", SystemUserName.ROOM);
            });
        }
        else {
            this.chat.socketMsg(socket, Errors.ALREADYINROOM, SystemUserName.SYSTEM);
        }
    }

    userLeaveRoom(socket: SocketIO.Socket, roomID: string) {
        let room = this.rooms.getRoom(roomID);
        if (room) {
            socket.leave(roomID, () => {
                room.userDisconnect(this.users.getUserBySocketId(socket.id));
                this.io.to(roomID).emit(SocketEvent.UPDATE, room.getUserCount(), roomID);
            });
        }
        this.rooms.deleteInactiveRooms();
    }

    // sync this client
    userSync(socket: SocketIO.Socket) {
        let user = this.users.getUserBySocketId(socket.id);
        if (!user) {
            return
        }
        let room = this.rooms.getRoom(user.getCurrentRoom());
        if (room) {
            room.clearSyncTime();
            this.io.to(user.getCurrentRoom()).emit(SocketEvent.SYNC_OTHER);
            setTimeout(function () {
                let time: number = room.getSyncTime()[0];
                //TODO 
                socket.emit(SocketEvent.SYNCTIME, time);
            }, 200);
        }
    }

    userApiRequestAllowed(socket: SocketIO.Socket) {
        let user = this.users.getUserBySocketId(socket.id);
        if (user && user.getAllowApiRequest()) {
            user.setLastApiRequest();
            return true;
        }
        return false;
    }

    // snyc other client
    syncUser(socket: SocketIO.Socket, time: number) {
        let user = this.users.getUserBySocketId(socket.id);
        if (!user) {
            return
        }
        let room = this.rooms.getRoom(user.getCurrentRoom());
        if (room) {
            room.syncClient(time);
        }
    }

    syncTime(socket: SocketIO.Socket, time: string) {
        let user = this.users.getUserBySocketId(socket.id);
        if (!user) {
            return
        }
        let room = this.rooms.getRoom(user.getCurrentRoom());
        if (room) {
            if (room.getIsEditable() || room.isOwner(user.getUserName())) {
                this.io.to(user.getCurrentRoom()).emit(SocketEvent.SYNCTIME, time);
                this.chat.roomMsg(user.getCurrentRoom(), user.getUserName() + ' synced Time at ' + time, null);
            } else {
                this.chat.socketMsg(socket, Errors.ROOMNOTEDITABLE, SystemUserName.SYSTEM);
            }
        }
    }

    adminMsgAll() {

    }

    adminSetApiKey(socket: SocketIO.Socket, pw: string, key: string) {

    }

    adminDeleteRoom(socket: SocketIO.Socket, pw: string, roomID: string) {
        if (this.admin.checkPassword(pw)) {
            this.rooms.deleteRoom(roomID);
            socket.emit(SocketEvent.PUBLIC_ROOMS, this.rooms.getPublicRooms());
        }
    }

    adminDeleteUser(socket: SocketIO.Socket, pw: string, username: string) {
        if (this.admin.checkPassword(pw)) {
            let user = this.users.getUserByUserName(username);
            if (!user.getStatus()) {
                this.io.to(user.getCurrentSocketId()).emit(SocketEvent.ALERT, "Admin deleted your Account. If you can see this, please reload the page to restore it!");
                this.users.deleteUser(user.getUserId());
                socket.emit(SocketEvent.GETUSERS, this.users.getAll());
            }
            else {
                socket.emit(SocketEvent.ALERT, "User still online!");
            }
        }
    }

    adminGetUsers(socket: SocketIO.Socket, pw: string) {
        if (this.admin.checkPassword(pw)) {
            socket.emit(SocketEvent.GETUSERS, this.users.getAll());
        }
    }

    adminGetRooms(socket: SocketIO.Socket, pw: string) {
        if (this.admin.checkPassword(pw)) {
            socket.emit(SocketEvent.GETROOMS, this.rooms.getAll());
        }
    }

    getPublicRooms(socket: SocketIO.Socket) {
        socket.emit(SocketEvent.PUBLIC_ROOMS, this.rooms.getPublicRooms());
    }

}
