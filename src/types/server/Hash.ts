export class Hash {
	private bcrypt: any;
	constructor() { }
	hashPW(pw: string) {
		if (pw) {
			var hash = 0, i, chr;
			if (pw.length === 0) return hash;
			for (i = 0; i < pw.length; i++) {
				chr = pw.charCodeAt(i);
				hash = ((hash << 5) - hash) + chr;
				hash |= 0; // Convert to 32bit integer
			}
			return hash;
		}
		return null;
	}
}