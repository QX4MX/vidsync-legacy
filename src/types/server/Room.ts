import { User } from "./User";
import { Video } from "./Video";

export class Room {

    private name: string;
    private createdBy: string;
    private userCount: number = 0;
    private created: Date = new Date();
    private lastUsed: Date;
    private isPerm: boolean;
    private isPublic: boolean;
    private isEditable: boolean;
    private currentVideo:Video;
    private currentRelatedVids: Array<Array<string>>;
    private queue: string[] = [];
    private queueCount: number = 0;
    private syncTime: number[];
    private lastFinish: Date = new Date();

    public constructor(name: string, createdBy: string, isPerm: boolean, isPublic: boolean, isEditable: boolean, StartVid: Video) {
        this.name = name;
        this.createdBy = createdBy;
        this.lastUsed = new Date();
        this.isPerm = isPerm;
        this.isPublic = isPublic;
        this.isEditable = isEditable;
        this.currentVideo = StartVid;
        console.log('New Room: ' + name + ' by ' + createdBy + ' (perm:' + isPerm, 'public:', isPublic, 'edit:', isEditable + ')');
    }

    public userConnect(user: User): void {
        console.log(user.getUserName(), ' joined Room ', this.name);
        this.setLastUsed(new Date());
        this.userCount++;
    }

    public userDisconnect(user: User): void {
        console.log(user.getUserName(), ' left Room ', this.name);
        this.userCount--;
    }

    public vidFinish() {
        if (this.lastFinish.getTime() < new Date().getTime() - 5) {
            this.lastFinish = new Date();
            return true;
        }
        return false;
    }

    public addToQueue(id: string): void {
        this.queue[this.queue.length] = id;
        this.queueCount++;

    }

    public removeFromQueue(qElem: number) {
        this.queueCount--;
        this.queue.splice(qElem, 1);
    }

    public checkIfActive(): boolean {
        let currentdate = new Date();
        if (this.userCount <= 0 && this.getIsPerm() == false) {
            return false;
        }
        return true;
    }

    isOwner(username: string) {
        if (username == this.createdBy) {
            return true;
        }
        return false;
    }

    clearSyncTime() {
        this.syncTime = [];
    }
    syncClient(time: number) {
        this.syncTime[this.syncTime.length] = time;
    }

    public getName(): string { return this.name; }

    public getCreatedBy(): string { return this.createdBy };

    public getUserCount(): number { return this.userCount; }

    public getCreated() { return this.created; }

    public getLastUsed(): Date { return this.lastUsed; }

    public getIsPerm(): boolean { return this.isPerm; }

    public getIsPublic(): boolean { return this.isPublic };

    public getIsEditable(): boolean { return this.isEditable };

    public getCurrentVideo():Video{ return this.currentVideo}; 

    public getCurrentUrl(): string { 
        if(this.currentVideo){
            return this.currentVideo.getId();
        }
        return null;
    }

    public getQueue(): string[] { return this.queue; }

    public getQueueCount(): number { return this.queueCount; }

    public getSyncTime(): number[] { return this.syncTime; }
    
    public playFromQueue(qElem: number) { 
        let queElem = this.queue[qElem];
        this.removeFromQueue(qElem);
        return queElem; 
    }

    public getQueueEmpty(){
        if(this.queueCount == 0){
            return true;
        }
        return false;
    }
    public getNext(){return this.queue[0]};

    public getRelatedVids(): Array<Array<string>> { return this.currentRelatedVids; }

    public setCreatedBy(createdBy: string) {
        this.createdBy = createdBy;
    }
    public setLastUsed(date: Date): void {
        this.lastUsed = date;
    }

    public setIsPerm(isPerm: boolean): void {
        this.isPerm = isPerm;
    }

    public setRelatedVids(vids: Array<Array<string>>) {
        this.currentRelatedVids = vids;
    }

    public setCurrentVideo(video:Video){
        this.currentVideo = video;
    }


}