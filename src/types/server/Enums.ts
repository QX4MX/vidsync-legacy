export enum SocketEvent {
    CONNECT = "connection",
    DISCONNECT = "disconnect",
    UPDATE = 'update',
    JOIN = 'join',
    MSG = 'msg',
    ALERT = 'alert',
    SET_USERNAME = 'setUserName',
    SET_ID = 'setId',
    LOAD_VID = 'loadId',
    PLAY = 'play',
    PAUSE = 'pause',
    STOP = 'stop',
    NEXT = 'next',
    FINISH = 'finish',
    ADD_TO_QUEUE = 'addToQ',
    RM_QUEUE_ELEM = 'removeQEl',
    PLAY_QUEUE_ELEM = 'playFromQ',
    LOAD_QUEUE = 'loadQueue',
    SYNCTIME = 'syncTime',
    SYNC_THIS = 'syncMe',
    SYNC_OTHER = 'syncClient',
    PUBLIC_ROOMS = 'publicRooms',
    GETUSERS = 'getUsers',
    GETROOMS = 'getRooms',
    DELROOM = 'deleteroom',
    DELUSER = 'deleteuser',
    searchYT = 'searchYoutube',
    RELATEDVIDS = 'relatedVids',
    playlistVideos = 'playlistVids',
    VIDSTATS = 'vidStats',
    BACKTOINDEX = 'errorRerouteToIndex',
    LOAD_RELATED = 'showrelated',
}

export enum SystemUserName {
    SYSTEM = "SYSTEM",
    ROOM = "ROOM",
    INFO = "INFO",
    ADMIN = "ADMIN"
}

export enum ChatCommand {
    GETCMDS = "!commands",
    SET = "!set",
    ADD = "!add",
    URL = "!url",
    USERS = "!users",
    ROOMUSERS = "!roomUsers",
    UPTIME = "!uptime",
    ROOMUPTIME = "!roomUptime",
}

export enum Errors {
    ROOMNOTEDITABLE = "You dont have permissions to edit this room!",
    NAMENOTALLOWED = "This name is not allowed or already in use!",
    USERNAMESET = "Got assigned new Username because yours was illegal. You will still be able to change it.",
    QUEUEEMPTY = "There are no more Videos in Queue!",
    ALREADYINROOM = "You are already in that Room!",
    EMPTYMSG = "Try again and u get ban for da spam", // caderyn (jan 2020)
    ROOMLIMIT = "The RoomLimit has been reached already, sorry!",
    QUEUELIMIT = "The QueueLimit has been reached in this Room!",
    IDEMPTY = "Cant set empty url!",
    APIREQUESTDENIED = "Every user can make an api request every 5 seconds. You either already did or the api is not ready yet!",
}
