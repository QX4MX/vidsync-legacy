function timeToHtml(time: number, htmlEl: string) {
    let hour = Math.floor(time / 3600);
    let hStr = hour.toString() + ':';
    let minutes = Math.floor((time % 3600) / 60);
    let mStr = minutes.toString() + ':';
    let seconds = Math.round(time % 60);
    let sStr = seconds.toString();

    if (time < 3600) {
        hStr = '';
    }
    else if (hour < 10) {
        hStr = '0' + hStr;
    }

    if (minutes < 10) {
        mStr = '0' + mStr;
    }
    if (seconds < 10) {
        sStr = '0' + sStr;
    }
    let str = hStr + mStr + sStr;
    $(htmlEl).html(str);
}

function fullScreen(docElm: any) {
    if (docElm.requestFullscreen) {
        docElm.requestFullscreen();
    } else if (docElm.mozRequestFullScreen) { /* Firefox */
        docElm.mozRequestFullScreen();
    } else if (docElm.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
        docElm.webkitRequestFullscreen();
    } else if (docElm.msRequestFullscreen) { /* IE/Edge */
        docElm.msRequestFullscreen();
    }
}

function exitFullScreen() {
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    } else if (document.webkitCancelFullScreen) {
        document.webkitCancelFullScreen();
    }
}



function getParamFromUrl(urlString: string, param: string) {
    let paramVal = null;
    try {
        let url = new URL(urlString);
        paramVal = url.searchParams.get(param);
    }
    catch (error) {
    }
    return paramVal;
}

function copyToClipboard(text: string) {
    let $temp = $("<input>");
    $("body").append($temp);
    $temp.val(text).select();
    document.execCommand("copy");
    $temp.remove();
}



