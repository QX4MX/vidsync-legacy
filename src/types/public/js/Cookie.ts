class Cookie {
    private username: string;
    private id: string;
    constructor() {

    }
    setCookie(cname: string, cvalue: string, exdays: number) {
        let d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        let expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    getCookie(cname: string) {
        let name = cname + "=";
        let ca = document.cookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }


    checkCookie() {
        this.id = this.getCookie("id");
        this.username = this.getCookie("username");
        if (this.username != "") {
        }
        else {
            this.setCookie("username", "Guest", 365);
            this.username = "Guest";
        }
    }

    getUserName(): string {
        this.checkCookie();
        return this.username;
    }
    getId(): string {
        return this.id;
    }
}