$(document).ready(function () {
    let joinInt = setInterval(function () {
        if (socketClass.getIsReady()) {
            socket = socketClass.getSocket();
            clearInterval(joinInt);
        }
    }, 500);

    $(document).on("click", "button.roomdel", function () {
        let roomid = <string>$(this).attr('id');
        let pw = prompt("Please enter the Admin password", "");
        if (pw == null || pw == "") {
        } else {
            socket.emit(SocketEvent.DELROOM, pw, roomid);
        }
    });

    $(document).on("click", "button.userdel", function () {
        let username = <string>$(this).attr('id');
        let pw = prompt("Please enter the Admin password", "");
        if (pw == null || pw == "") {
        } else {
            socket.emit(SocketEvent.DELUSER, pw, username);
        }
    });

    $('#refreshU').click(function () {
        let pw = prompt("Please enter the Admin password", "");
        if (pw == null || pw == "") {
        } else {
            socket.emit(SocketEvent.GETUSERS, pw);
        }
    });

    $('#refreshR').click(function () {
        let pw = prompt("Please enter the Admin password", "");
        if (pw == null || pw == "") {
        } else {
            socket.emit(SocketEvent.GETROOMS, pw);
        }
    });
});


socket.on(SocketEvent.GETROOMS, function (rooms: any) {
    $('#rooms').empty();
    for (let room of rooms) {
        for (let i = 0; i < room.length; i++) {
            if (room[i]) {
                room[i] = room[i].replace("true", '<i class="fas fa-check"></i>');
                room[i] = room[i].replace("false", '<i class="fas fa-times"></i>');
            }
        }
        $('#rooms').append("<tr class='hover-bg-dark'><td>" + room[0] + "</td><td>" + room[1] + "</td><td>" + room[2] + "</td><td>" + room[3] + "</td><td>" + room[4] + "</td><td>" + room[5] + "</td><td>" + room[6] + "</td><td>" + room[7] + "</td><td>" + room[8] + "</td><td><button class='bg-dark border-primary hover-bg-primary rounded'><a href=" + location.origin + '/room?id=' + room[0] + " class='c-light hover-bg-primary'>Join Room</a></button></td>\
        <td><button id="+ room[0] + " class='roomdel bg-dark border-primary hover-bg-primary rounded c-light hover-bg-primary'>Delete Room </button></td></tr>");
    }
});

socket.on(SocketEvent.GETUSERS, function (users: any) {
    $('#users').empty();
    for (let user of users) {
        $('#users').append("<tr class='hover-bg-dark'><td>" + user[0] + "</td><td>" + user[1] + "</td><td>" + user[2] + "</td><td>" + user[3] + "</td><td>" + user[4] + "</td><td><button id=" + user[0] + " class='userdel bg-dark border-primary hover-bg-primary rounded c-light hover-bg-primary'>Delete User </button></td></tr>");
    }
});

