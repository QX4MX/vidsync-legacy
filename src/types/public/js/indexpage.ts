$(document).ready(function () {
    let joinInt = setInterval(function () {
        if (socketClass.getIsReady()) {
            socket = socketClass.getSocket();
            clearInterval(joinInt);
        }
    }, 500)

    $('#privacyVal').html("Private");
    $('#privacyDesc').html("Users can only join with a link");

    $('#privacyVal').click(function () {
        if($('#privacyVal').html() == "Private"){
            $('#privacyVal').html("Public");
            $('#privacyDesc').html("Room is shown to all users");
        }
        else{
            $('#privacyVal').html("Private");
            $('#privacyDesc').html("Users can only join with a link");
        }

    });
    
    $('#permVal').html("Temporary");
    $('#permDesc').html("System WILL delete Room after all users leave.");

    $('#permVal').click(function () {
        if($('#permVal').html() == "Permanent"){
            $('#permVal').html("Temporary");
            $('#permDesc').html("System WILL delete Room after all users leave.");
        }
        else{
            $('#permVal').html("Permanent");
            $('#permDesc').html("System will NOT delete Room after all users leave.");
        }
    });

    $('#editVal').html("Editable");
    $('#editDesc').html("Allows other users to change video in Room.");

    $('#editVal').click(function () {
        if($('#editVal').html() == "Editable"){
            $('#editVal').html("Not Editable");
            $('#editDesc').html("Other users can not change video in Room.");
        }
        else{
            $('#editVal').html("Editable");
            $('#editDesc').html("Allows other users to change video in Room.");
        }
    });

    $('#newUserName').click(function () {
        let newUserName = $('#newUserNameVal').val();
        socket.emit(SocketEvent.SET_USERNAME, newUserName);
    });

    $('#joinRoom').click(function () {
        let roomId = $('#roomVal').val();
        let isPublic = ($('#privacyVal').html() == "Public");
        let isPerm = ($('#permVal').html() == "Permanent");
        let isEditable = ($('#editVal').html() == "Editable");
        window.location.href = '/room?id=' + roomId + '&pub=' + isPublic + '&perm=' + isPerm + '&e=' + isEditable;
    });
});

$('form').submit(function (e) {
    e.preventDefault(); // prevents page reloading
    return false;
});

socket.on(SocketEvent.PUBLIC_ROOMS, function (rooms: any) {
    $('#pubRooms').empty();
    for (let room of rooms) {
        for (var i = 0; i < room.length; i++) {
            if (room[i]) {
                room[i] = room[i].replace("true", '<i class="fas fa-check"></i>');
                room[i] = room[i].replace("false", '<i class="fas fa-times"></i>');
            }
        }
        $('#pubRooms').append("<div class='bg-darker card'>\
        <img id='video-list-" + i + "'class='card-img' src='https://img.youtube.com/vi/" + room[2] + "/hqdefault.jpg'>\
        <a href=" + location.origin + '/room?id=' + room[0] + " class='card-title border-primary-bot c-light'>" + room[0] + "</span></a>\
        <div class='card-body'>\
        <span>Created by: " + room[1] + "</span>\
        <span>Users in Room: " + room[3] + "</span>\
        <span>Permanent: " + room[4] + "</span>\
        <span>Editable: " + room[5] + "</span>\
        </div><div class='card-bot'><button class='bg-dark border-primary hover-bg-primary rounded'><a href=" + location.origin + '/room?id=' + room[0] + " class='c-light'>Join Room</a></button>\
        </div>");
    }
    if(rooms.length == 0){
        $('#pubRoomsHeader').html('No public rooms available at this time.')
    }
    else{
        $('#pubRoomsHeader').html('Public Rooms')
    }
});